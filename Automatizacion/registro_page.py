

from locators import locatorspage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
import time
from faker import Faker

fake=Faker()


class registropage():
    
    fake=Faker()
    
    def __init__(self, driver):
        self.driver = driver   
        
    def IngresarIcono_registro(self):
        time.sleep(5)
        self.driver.find_element(*locatorspage.Login_icon).click()
        time.sleep(2)
        
    def CrearCuenta_Registro(self):
        registrar = self.driver.find_element(*locatorspage.Newaccount_button)
        ActionChains(self.driver).move_to_element(registrar).click().perform()
        time.sleep(2)    
        
    def IngresarNombreUsuario_registro(self):
        Nombre_Usuario = fake.last_name()
        self.driver.find_element(*locatorspage.UsernameR_box).send_keys(Nombre_Usuario)
        print(Nombre_Usuario)
        time.sleep(2)
        
    def IngresarCorreo_Registro(self):
        self.driver.find_element(*locatorspage.EmailR_box).send_keys(fake.email())
        time.sleep(2)
        
    def IngresarContraseña_registro(self):
        Contraseña = fake.password(length=8,special_chars=True,digits=True, upper_case=True,lower_case=True)
        self.driver.find_element(*locatorspage.PasswordR_box).send_keys(Contraseña)
        self.driver.find_element(*locatorspage.ConfirmPasswordR_box).send_keys(Contraseña)
        print(Contraseña)
        time.sleep(2)
        
    def IngresarNombre_registro(self):
        self.driver.find_element(*locatorspage.FirstNameR_box).send_keys(fake.first_name())
        time.sleep(2)
        
    def IngresarApellido_registro(self):
        self.driver.find_element(*locatorspage.LastNameR_box).send_keys(fake.last_name())
        time.sleep(2)
        
    def IngresarTelefono_registro(self):
        self.driver.find_element(*locatorspage.PhoneNumberR_box).send_keys(fake.phone_number())
        time.sleep(2)
        
    def IngresarPais_registro(self):
        self.driver.execute_script("window.scrollTo(0, 450);")
        time.sleep(2)
        self.driver.find_element(*locatorspage.CountryR_Lista).click() 
        self.driver.find_element(*locatorspage.Seleccionar_pais).click() 
        time.sleep(2) 
        
        
    def IngresarCiudad_registro(self):
        self.driver.find_element(*locatorspage.CityR_box).send_keys(fake.city())
        time.sleep(2)
        
    def IngresarDireccion_registro(self):
        self.driver.find_element(*locatorspage.AddressR_box).send_keys(fake.city())
        time.sleep(2)
    
    def IngresarEstadoProvinciaRegion_registro(self):
        self.driver.find_element(*locatorspage.State_Province_regionR_box).send_keys(fake.last_name())
        time.sleep(2)
        
    def IngresaCodigopostal_registro(self):
        self.driver.find_element(*locatorspage.PostalcodeR_box).send_keys(fake.postcode())
        time.sleep(2)
        #self.driver.execute_script("window.scrollTo(0, 300);")
        #time.sleep(2)
    
    def Aceptarterminos(self):
        self.driver.find_element(*locatorspage.IagreeR_checkbox).click()
        time.sleep(2)
        
    def Registrarse(self):
        self.driver.execute_script("window.scrollTo(0, 700);")
        time.sleep(2)
        self.driver.find_element(*locatorspage.RegisterR_button).click()
        time.sleep(5)
            