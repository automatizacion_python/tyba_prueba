import unittest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from faker import Faker
from registro_page import registropage
from agregaralcarritopages import Agregaralcarrito

class TestRegistration(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

    def test_registration_success(self):
        driver = self.driver
        registro = registropage(driver)
        self.driver.get('https://www.advantageonlineshopping.com/#/')
        registro.IngresarIcono_registro()
        registro.CrearCuenta_Registro()
        driver.save_screenshot("/Users/lilianaramirez/Desktop/Tyba/Automatizacion/screenshot/Registar.png")
        registro.IngresarNombreUsuario_registro()
        registro.IngresarCorreo_Registro()
        registro.IngresarContraseña_registro()
        registro.IngresarNombre_registro()
        registro.IngresarApellido_registro()
        registro.IngresarTelefono_registro()
        driver.save_screenshot("/Users/lilianaramirez/Desktop/Tyba/Automatizacion/screenshot/primeros_datos.png")
        registro.IngresarPais_registro()
        registro.IngresarCiudad_registro()
        registro.IngresarDireccion_registro()
        registro.IngresarEstadoProvinciaRegion_registro()
        registro.IngresaCodigopostal_registro()
        driver.save_screenshot("/Users/lilianaramirez/Desktop/Tyba/Automatizacion/screenshot/segundos_datos.png")
        registro.Aceptarterminos()
        registro.Registrarse()
        
   
        driver = self.driver
        carrito= Agregaralcarrito(driver)
        carrito.ClicKPopularItems()
        carrito.ClickProducto()
        carrito.AgregarAl_Carrito()
        driver.save_screenshot("/Users/lilianaramirez/Desktop/Tyba/Automatizacion/screenshot/carrito.png")
        
           
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()