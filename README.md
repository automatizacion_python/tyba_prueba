Los scripts construidos se encuentran en la carpeta Automatizacion, el lenguaje utilizado Python mediante Visual Studio Code
Se encuentra estructurado bajo el patron de diseño Page Object Model de la siguiente manera:

1) Los metodos se construyen bajo los siguientes archivos:
a) registro_page.py
b) agregaralcarritopage.py
c) pagarpage.py
2) Los Localizadores se encuentran en el archivo locators.py.
3) Para la ejecucion de los test se debe hacer desde el archivo test.py (se realizan dos pruebas especificas, la primera frente al formulario completo registro y el agregar un producto al carrito de compras)
4) Se encuentra tambien una carpeta con el nombre Screenshot con algunas evidencias tomadas durante la automatizacion

