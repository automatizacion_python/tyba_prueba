
from selenium.webdriver.common.by import By

class locatorspage(object):
    
    #Iniciar sesion
    Login_icon = (By.ID, "menuUser")
    Username_box = (By.NAME, "username")
    Password_box = (By.NAME, "password")
    Singin_button = (By.ID, "sign_in_btnundefined")
    #Registrarse
    Newaccount_button = (By.XPATH, "//a[@href='javascript:void(0)'][contains(.,'CREATE NEW ACCOUNT')]")
    UsernameR_box = (By.NAME, "usernameRegisterPage")
    EmailR_box = (By.NAME, "emailRegisterPage")
    PasswordR_box = (By.NAME, "passwordRegisterPage")
    ConfirmPasswordR_box = (By.NAME, "confirm_passwordRegisterPage")
    FirstNameR_box = (By.NAME, "first_nameRegisterPage")
    LastNameR_box = (By.NAME, "last_nameRegisterPage")
    PhoneNumberR_box = (By.NAME, "phone_numberRegisterPage")
    
    Seleccionar_pais= (By.NAME, "countryListboxRegisterPage")
    CountryR_Lista =(By.XPATH, "//*[@id='formCover']/div[3]/div[1]/sec-view[1]/div/select/option[45]") 

    CityR_box = (By.NAME, "cityRegisterPage")
    AddressR_box = (By.NAME, "addressRegisterPage")
    State_Province_regionR_box = (By.NAME, "state_/_province_/_regionRegisterPage")
    PostalcodeR_box = (By.NAME, "postal_codeRegisterPage")
    IagreeR_checkbox = (By.NAME, "i_agree")
    RegisterR_button = (By.ID, "register_btnundefined")
    #opciones menu header
    PopularItems_button = (By.XPATH, "/html/body/header/nav/ul/li[6]/a") #roboto-light desktop-handler
    #Seleccionar popular item
    HP_ROAR_button = (By.ID, "details_21")
    Color_Selector = (By.CLASS_NAME, "bunny productColor ng-scope PURPLE")
    Addtocart_button = (By.NAME, "save_to_cart")
    speakers_button = (By.CLASS_NAME, "ng-binding")
    speakers_Lista = (By.NAME, "cell categoryRight")
    BoseSoundLink_button = (By.ID, "25")
    Increment_value = (By.CLASS_NAME, "plus")
    MenuCart_icon = (By.ID, "menuCart")
    Checkout_button = (By.NAME, "check_out_btn")
    #pagar
    Next_button = (By.ID, "next_btn")
    Safename_box = (By.NAME, "safepay_username")
    Safepassword_box = (By.NAME, "safepay_password")
    Pay_now = (By.ID, "pay_now_btn_SAFEPAY")
    OrderPayment_text = (By.CLASS_NAME, "roboto-regular sticky fixedImportant ng-scope fixed")