
import time
from locators import locatorspage
from faker import Faker

class PagarPages():
    
    def __init__(self, driver):
        self.driver = driver  
    
    def SiguientePaso_EnPago(self):
        self.driver.find_element(*locatorspage.Next_button).click()
        time.sleep(2)
        
    def IngresarNombre_Pago(self):
        self.driver.find_element(*locatorspage.Safename_box).send_keys(Faker.user_name(length=8,special_chars=False,
                                                                    digits=False, upper_case=True,lower_case=True))
    
    def IngresarContraseña_Pago(self):
        self.driver.find_element(*locatorspage.Safepassword_box).send_keys(Faker.password(length=8,special_chars=True,
                                                                     digits=True, upper_case=True,lower_case=True))
    
    def Pagar(self):
        self.driver.find_element(*locatorspage.Pay_now).click()
        time.sleep(2)
    