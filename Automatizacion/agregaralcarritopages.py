from locators import locatorspage
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
import time

class Agregaralcarrito():

    def __init__(self, driver):
        self.driver = driver  

    def ClicKPopularItems(self):
        self.driver.find_element(*locatorspage.PopularItems_button).click()
        #Item =lista.find_element_by_tag_name("li")
        #Item[6].click()
        time.sleep(5)
        
    def ClickProducto(self):
        self.driver.find_element(*locatorspage.HP_ROAR_button).click()
        time.sleep(2)
        
    def SeleccionarColor(self):
        
        color=self.driver.find_element(*locatorspage.Color_Selector)
        ActionChains(self.driver).move_to_element(color).click().perform() 
        time.sleep(2) 
        
    def AgregarAl_Carrito(self):
        self.driver.find_element(*locatorspage.Addtocart_button).click()
        time.sleep(5)
    
    def Mas_Productos(self):
        productos=self.driver.find_element(*locatorspage.speakers_button)
        ActionChains(self.driver).move_to_element(productos).click().perform() 
        time.sleep(2)  
        
    def Lista_Productos(self):
        self.driver.execute_script("window.scrollTo(0, 450);")
        time.sleep(2)
        self.driver.find_element(*locatorspage.BoseSoundLink_button).click()
        time.sleep(2)
          
    def Incrementar_Productos(self):
        Incrementar = self.driver.find_element(*locatorspage.Increment_value)
        ActionChains(self.driver).move_to_element(Incrementar).click().click().click().perform()
        time.sleep(2) 
    
    def IralCarrito(self):
        self.driver.find_element(*locatorspage.MenuCart_icon).click()
        time.sleep(2) 
        
    def ir_a_pagar(self):
        self.driver.find_element(*locatorspage.Checkout_button).click()
        time.sleep(2)    
    
    
    